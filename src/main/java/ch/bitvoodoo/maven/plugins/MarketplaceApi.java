package ch.bitvoodoo.maven.plugins;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Properties;

class MarketplaceApi {
	private static final String MPACAPI = "https://marketplace.atlassian.com/rest/2/products/key/";

	private final Log log;
	private final Boolean cache;

	private File cacheFile;
	private Properties prop = new Properties();


	MarketplaceApi(Boolean cache, File cacheDirectory, Log log) {
		this.log = log;
		this.cache = cache;

		if (cache) {
			this.readCache(cacheDirectory);
		}
	}

	private void readCache(File cacheDirectory) {
		if (!cacheDirectory.exists()) {
			if (!cacheDirectory.mkdirs()) {
				log.warn("Can't read " + cacheDirectory.getAbsolutePath());
			}
		}
		cacheFile = new File(cacheDirectory, "atlassian-marketing-maven-plugin-cache.properties");
		if (cacheFile.exists() && DateUtils.isSameDay(new Date(), new Date(cacheFile.lastModified()))) {
			try {
				InputStream input = new FileInputStream(cacheFile);
				prop.load(input);

			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
	}

	private String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		log.info("Get MPAC REST API " + url);

		try (InputStream is = new URL(url).openStream()) {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return new JSONObject(jsonText);
		}
	}


	private String getBuildNumberFromUrl(String url) throws JSONException, IOException {
		return getFieldFromUrl(url, "buildNumber");

	}

	private String getVersionFromUrl(String url) throws JSONException, IOException {
		return getFieldFromUrl(url, "name");
	}

	private String getFieldFromUrl(String url, String field) throws JSONException, IOException {
		String cacheKey = (url + "-" + field).replace("https://", "");
		if (prop.containsKey(cacheKey)) {
			return prop.getProperty(cacheKey);
		}

		JSONObject jsonObject = this.readJsonFromUrl(url);

		if (jsonObject.has(field)) {
			String value = jsonObject.getString(field);
			this.updateCache(cacheKey, value);
			return value;
		}
		return null;
	}

	String getBuildNumber(String product, String version) throws JSONException, IOException {
		return this.getBuildNumberFromUrl(MPACAPI + product + "/versions/name/" + version);
	}

	String getLatestBuildNumber(String product) throws JSONException, IOException {
		return this.getBuildNumberFromUrl(MPACAPI + product + "/versions/latest");
	}

	String getLatestVersion(String product) throws JSONException, IOException {
		return this.getVersionFromUrl(MPACAPI + product + "/versions/latest");
	}

	private void updateCache(String key, String value) {
		prop.setProperty(key, value);

		if (cache) {
			try {
				if (!cacheFile.exists()) {
					if (!cacheFile.createNewFile()) {
						log.warn("Can't read " + cacheFile.getAbsolutePath());
					}
				}

				OutputStream output = new FileOutputStream(cacheFile);
				prop.store(output, null);
			} catch (IOException e) {
				log.warn("Error writing cache: " + e.getMessage());
			}
		}
	}
}