package ch.bitvoodoo.maven.plugins;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.json.JSONException;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

@Mojo(name = "create", defaultPhase = LifecyclePhase.INITIALIZE)
@SuppressWarnings("unused")
public class AtlassianMarketingFileMojo
		extends AbstractMojo {


	@Parameter(property = "product", required = true)
	private String product;

	@Parameter(property = "confluenceMinVersion", defaultValue = "${confluence.version}")
	private String confluenceMinVersion;

	@Parameter(property = "confluenceMaxVersion")
	private String confluenceMaxVersion;


	@Parameter(property = "bambooMinVersion", defaultValue = "${bamboo.version}")
	private String bambooMinVersion;

	@Parameter(property = "bambooMaxVersion")
	private String bambooMaxVersion;

	@Parameter(property = "bitbucketMinVersion", defaultValue = "${bitbucket.version}")
	private String bitbucketMinVersion;

	@Parameter(property = "bitbucketMaxVersion")
	private String bitbucketMaxVersion;

	@Parameter(property = "jiraMinVersion", defaultValue = "${jira.version}")
	private String jiraMinVersion;

	@Parameter(property = "jiraMaxVersion")
	private String jiraMaxVersion;


	@Parameter(readonly = true, defaultValue = "${project.build.outputDirectory}")
	private File outputDirectory;


	@Parameter(readonly = true, defaultValue = "${project.build.directory}")
	private File cacheDirectory;

	@Parameter(readonly = true, defaultValue = "atlassian-plugin-marketing.xml")
	private String outputFile;

	@Parameter(readonly = true, defaultValue = "UTF-8")
	private String encoding;

	@Parameter(readonly = true, defaultValue = "false")
	private Boolean verbose;


	@Parameter(readonly = true, defaultValue = "true")
	private Boolean cache;

	@Parameter(readonly = true, defaultValue = "false")
	private Boolean buildFailOnError;

	private final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();


	public void execute()
			throws MojoExecutionException {


		if (!outputDirectory.exists()) {
			if (!outputDirectory.mkdirs()) {
				throw new MojoExecutionException("Can't read " + outputDirectory.getAbsolutePath());
			}
		}

		MarketplaceApi marketplaceApi = new MarketplaceApi(cache, cacheDirectory, getLog());

		File file = new File(outputDirectory, outputFile);
		String minBuild;
		String maxBuild;

		try {

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("atlassian-plugin-marketing");
			doc.appendChild(rootElement);

			// compatibility element
			Element compatibility = doc.createElement("compatibility");
			rootElement.appendChild(compatibility);


			for (String atlasApp : product.split(",")) {

				switch (atlasApp.trim().toLowerCase()) {
					case "confluence":
						// Confluence uses build numbers
						if (StringUtils.isBlank(confluenceMinVersion)) {
							throw new IllegalArgumentException("No <confluenceMinVersion> defined nor ${confluence.version} could be resolved");
						}
						minBuild = marketplaceApi.getBuildNumber(atlasApp, confluenceMinVersion);
						if (StringUtils.isBlank(confluenceMaxVersion)) {
							maxBuild = marketplaceApi.getLatestBuildNumber(atlasApp);
						} else {
							maxBuild = marketplaceApi.getBuildNumber(atlasApp, confluenceMaxVersion);
						}
						break;
					case "jira":

						if (StringUtils.isBlank(jiraMinVersion)) {
							throw new IllegalArgumentException("No <jiraMinVersion> defined nor ${jira.version} could be resolved");
						}
						minBuild = jiraMinVersion;
						if (StringUtils.isBlank(jiraMaxVersion)) {
							maxBuild = marketplaceApi.getLatestVersion(atlasApp);
						} else {
							maxBuild = jiraMaxVersion;
						}
						break;
					case "bamboo":
						if (StringUtils.isBlank(bambooMinVersion)) {
							throw new IllegalArgumentException("No <bambooMinVersion> defined nor ${bamboo.version} could be resolved");
						}
						minBuild = bambooMinVersion;
						if (StringUtils.isBlank(bambooMaxVersion)) {
							maxBuild = marketplaceApi.getLatestVersion(atlasApp);
						} else {
							maxBuild = bambooMaxVersion;
						}
						break;
					case "bitbucket":
						if (StringUtils.isBlank(bitbucketMinVersion)) {
							throw new IllegalArgumentException("No <bitbucketMinVersion> defined nor ${bitbucket.version} could be resolved");
						}
						minBuild = bitbucketMinVersion;
						if (StringUtils.isBlank(bitbucketMaxVersion)) {
							maxBuild = marketplaceApi.getLatestVersion(atlasApp);
						} else {
							maxBuild = bitbucketMaxVersion;
						}
						break;
					default:
						throw new IllegalArgumentException("No supported atlasApp: " + atlasApp);

				}

				// compatibility element
				Element productTag = doc.createElement("product");
				compatibility.appendChild(productTag);

				Attr attr = doc.createAttribute("max");
				attr.setValue(maxBuild);
				productTag.setAttributeNode(attr);

				attr = doc.createAttribute("min");
				attr.setValue(minBuild);
				productTag.setAttributeNode(attr);

				// set attribute to staff element
				attr = doc.createAttribute("name");
				attr.setValue(atlasApp);
				productTag.setAttributeNode(attr);

				getLog().info("product: " + atlasApp + " min: " + minBuild + " max: " + maxBuild);

			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);

			StringWriter writer = new StringWriter();
			StreamResult resultString = new StreamResult(writer);


			transformer.transform(source, result);
			transformer.transform(source, resultString);


			if (verbose) {
				getLog().info(writer.toString());
			}

		} catch (ParserConfigurationException | TransformerException | JSONException | IOException | IllegalArgumentException pce) {
			getLog().error(pce.getMessage());
			if (buildFailOnError) {
				throw new MojoExecutionException(pce.getMessage());
			}
		}
	}
}
